'use strict';

angular.module('wixDashboard', [
    'ngRoute',
    'wixForPage',
    'pageGuard'
])
        .config(function ($routeProvider) {
            $routeProvider
                    .when('/', {
                        templateUrl: 'views/dashboard.html',
                        controller: 'MainCtrl'
                    })
                    .otherwise({
                        redirectTo: '/'
                    });
        });
        
