'use strict';

angular.module('wixApp')
        .controller('MainCtrl', function ($scope, $wixForWorker, $pageGuard, $location) {

            $scope.handleEvent = function (event) {
                $scope.$apply(function () {
                    $scope.message = event;
                });
            };

            //Worker should listen for user auth or subscribe and disable
            //page redirect if received
            $wixForWorker.Worker.PubSub.subscribe("user_authed", function (event) {
                $pageGuard.setAuthUser(event.data.authId);
                return;
            }, true);

            $scope.mainPage = '';
            $scope.blockerPage = '';

            $scope.getSettings = function (instance) {
                var setRet = $pageGuard.retrieveSettings(instance);
            };

            $wixForWorker.Worker.getSitePages(function (sp) {
                $scope.mainPage = sp[Object.keys(sp)[0]];
                $scope.redirectTo = sp[Object.keys(sp)[0]];
                $wixForWorker.Worker.getSiteInfo(function (i) {
                    angular.forEach(sp, function (v, k) {
                        if (i.pageTitle === v.title) {
                            $scope.currentPage = v.id;
                        }
                        if (v.title === "Page Guard") {
                            $scope.blockerPage = v.id;
                        }
                        angular.forEach(v.subPages, function (vv, kk) {
                            if (i.pageTitle === vv.title) {
                                $scope.currentPage = vv.id;
                            }
                            if (vv.title === "Page Guard") {
                                $scope.blockerPage = vv.id;
                            }
                        });
                    });
                    $scope.checkIfPageIsGuarded($scope.currentPage);
                });
            });

            $scope.setGuard = function (page_identifier) {
                if ($pageGuard.getAuthUser().uid !== 0) {
                    return;
                }
                $wixForWorker.Worker.PubSub.publish("page_blocked", {id: page_identifier}, true);
                $wixForWorker.Worker.Utils.navigateToSection($scope.blockerPage);
            };

            $scope.checkIfPageIsGuarded = function (page_identifier)
            {
                if ($pageGuard.checkRestricted(page_identifier)) {
                    $scope.setGuard(page_identifier);
                }
            };

            $scope.handlePageNavigation = function (event) {
                $scope.checkIfPageIsGuarded(event.toPage);
            };

            $wixForWorker.Worker.addEventListener($wixForWorker.Events.SETTINGS_UPDATED, $scope.handleEvent);

            $wixForWorker.Worker.addEventListener($wixForWorker.Events.PAGE_NAVIGATION, $scope.handlePageNavigation);

            $scope.viewModeCheck = $wixForWorker.Worker.Utils.getViewMode();

            $scope.runOnce = false;
            if ($scope.viewModeCheck !== 'standalone' && $scope.runOnce === false) {
                $scope.runOnce = true;
                $scope.instanceId = $wixForWorker.Worker.Utils.getInstanceId();
                $scope.instance = $wixForWorker.Worker.Utils.getInstance();
                $scope.getSettings($scope.instance);
            }

        });