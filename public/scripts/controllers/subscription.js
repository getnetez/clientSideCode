'use strict';

angular.module('wixSubscription')
        .controller('MainCtrl', function ($scope, $wixForPage, $pageGuard, $location, $sce, $interval, localStorageService) {

            $scope.handleEvent = function (event) {
                $scope.$apply(function () {
                    $scope.message = event;
                });
            };

            $scope.plans = {};

            $scope.plans.activePlans = [];
            $scope.plans.selectedPlan = {};

            $scope.enteredEmail = '';
            $scope.enteredPw = '';
            $scope.userMessage = false;
            $scope.emailMessage = false;
            $scope.loginMessage = false;
            $scope.resetMessage = false;
            $scope.polling = false;
            $scope.triggerPolling = false;

            $scope.getPlans = function (instance) {
                var getPlans = $pageGuard.retrievePlans(instance);
                getPlans.then(function (result) {
                    var m = [];
                    angular.forEach(result.data, function (v, k) {
                        switch (v.opt_num) {
                            case 'sub_one':
                                m[0] = v;
                                break;
                            case 'sub_two':
                                m[1] = v;
                                break;
                            case 'sub_three':
                                m[2] = v;
                                break;
                            case 'sub_four':
                                m[3] = v;
                                break;
                        }
                    });
                    angular.forEach(m, function (v) {
                        $scope.plans.activePlans.push(v);
                    });
                    $scope.plans.activePlans = result.data;
                    $scope.plans.selectedPlan = $scope.plans.activePlans[0];
                });
            };


            function validateEmail(email) {
                var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                return re.test(email);
            }

            $scope.checkEmail = function (inp) {
                $scope.emailMessage = false;
                $scope.userMessage = false;
                $scope.triggerPolling = false;
                if (inp.value !== '' && !validateEmail(inp.value)) {
                    $('input[name="email_address"').val('');
                    $scope.emailMessage = true;
                    return;
                }
                $pageGuard.checkEmailExists(inp.value, $scope.instance).success(function (res) {
                    if (res.success === 'true') {
                        $scope.existingEmail = inp.value;
                        $scope.userMessage = true;
                        return;
                    }
                    $scope.enteredEmail = inp.value;
                    if ($scope.enteredEmail !== '' && $scope.enteredPw !== '') {
                        $pageGuard.setNewUser($scope.enteredEmail, $scope.enteredPw,
                                $scope.instance);
                        $scope.triggerPolling = true;
                    }
                });
            };

            $scope.checkPw = function (inp) {
                $scope.enteredPw = inp.value;
                if ($scope.enteredEmail !== '' && $scope.enteredPw !== '') {
                    $pageGuard.setNewUser($scope.enteredEmail, $scope.enteredPw,
                            $scope.instance);
                    $scope.triggerPolling = true;
                }

                if ($scope.triggerPolling) {
                    var signupRedirector = $interval(function () {
                        $pageGuard.checkCredentials(
                                $scope.enteredEmail, $scope.enteredPw,
                                $scope.instance, $scope.viewModeCheck).success(function (res) {
                            if (res.success === 'true') {
                                localStorageService.set('remUser', res.user);
                                $pageGuard.setAuthUser(res.user);
                                $wixForPage.PubSub.publish("user_authed", {authId: res.user}, true);
                                $interval.cancel(signupRedirector);
                                $wixForPage.navigateToPage($scope.redirectTo.id);
                            }
                        });
                    }, 2500, 150);
                }
            };

            $scope.$location = $location;
            $scope.$sce = $sce;

            $scope.cred = {};

            // Subscription page should listen for a redirect 
            // because of page block and be ready to redirect 
            // properly on auth or subscribe
            $wixForPage.PubSub.subscribe("page_blocked", function (event) {
                $scope.redirectTo = event.data;
                return;
            }, true);

            $scope.getSettings = function (instance) {
                var setRet = $pageGuard.retrieveSettings(instance);
                setRet.then(function (result) {
                    $scope.Settings = result;
                });
            };

            $wixForPage.Settings.getSitePages(function (sp) {
                $scope.mainPage = sp.shift();
            });

            $scope.attemptLogin = function () {
                $scope.loginMessage = false;
                $pageGuard.checkCredentials(
                        $scope.cred.Id, $scope.cred.Pw,
                        $scope.instance, $scope.viewModeCheck).success(function (res) {
                    if (res.success === 'true') {
                        localStorageService.set('remUser', res.user);
                        $pageGuard.setAuthUser(res.user);
                        $wixForPage.PubSub.publish("user_authed", {authId: res.user}, true);
                        $wixForPage.navigateToPage($scope.redirectTo.id);
                    } else {
                        $scope.loginMessage = true;
                    }
                });
            };

            $scope.checkRemUser = function (instance, u) {
                $pageGuard.checkRemUser(instance, u).success(function (res) {
                    if (res.success === 'true') {
                        localStorageService.set('remUser', res.user);
                        $pageGuard.setAuthUser(res.user);
                        $wixForPage.PubSub.publish("user_authed", {authId: res.user}, true);
                        $wixForPage.navigateToPage($scope.redirectTo.id);
                    }
                });
            }
            ;

            $wixForPage.addEventListener($wixForPage.Events.SETTINGS_UPDATED, $scope.handleEvent);

            $scope.viewModeCheck = $wixForPage.Utils.getViewMode();

            $scope.runOnce = false;
            if ($scope.viewModeCheck !== 'standalone' && $scope.runOnce === false) {
                $scope.runOnce = true;
                $scope.instanceId = $wixForPage.Utils.getInstanceId();
                $scope.instance = $wixForPage.Utils.getInstance();
                $scope.getSettings($scope.instance);
                $scope.getPlans($scope.instance);
                if (localStorageService.get('remUser')) {
                    $scope.checkRemUser($scope.instance, localStorageService.get('remUser'));
                }
            }

            $scope.noThanks = function () {
                $scope.redirect = null;
                $wixForPage.navigateToPage($scope.mainPage.id);
            };

            $scope.passwordReset = function () {
                $scope.resetMessage = false;
                var email = $('input[name="email_address"').val('');
                $pageGuard.passwordreset(email, $scope.instance).success(function (res) {
                    if (res.success === 'true') {
                        $scope.resetMessage = true;
                    }
                });
            };

        }).filter('simpledecode', function () {
    return function (input) {
        var output = input || '';
        return output.replace(/&#([0-9]{1,3});/gi, function (match, numStr) {
            var num = parseInt(numStr, 10); // read num as normal number
            return String.fromCharCode(num);
        });
    };
});