'use strict';

angular.module('wixDashboard')
        .controller('MainCtrl', function ($scope, $wixForPage, $pageGuard) {
            $scope.Dashboard = {};
            $scope.Dashboard.title = 'PageGuard Subscriptions';
            
            $scope.getSubscriptions = function (instance, vm) {
                var getSubscriptions = $pageGuard.retrieveSubscriptions(instance, vm);
                getSubscriptions.then(function (result) {
                    $scope.Dashboard.subscriptions = result.data;
                });
            };
            
            $scope.viewModeCheck = $wixForPage.Utils.getViewMode();
            $scope.runOnce = false;
            if ($scope.viewModeCheck !== 'standalone' && $scope.runOnce === false) {
                $scope.runOnce = true;
                $scope.instanceId = $wixForPage.Utils.getInstanceId();
                $scope.instance = $wixForPage.Utils.getInstance();
                $scope.getSubscriptions($scope.instance, $scope.viewModeCheck);
            }
        });


