'use strict';

angular.module('appSettings')
        .controller('SettingsCtrl', function ($scope, $wixForPage, $pageGuard) {
            $scope.baseURL = "http://localhost:8000";

            $scope.params = {};

            $scope.viewVars = {};

            $scope.subscriptions = {
                PayPalIsConnected: false,
                PremiumApp: false,
                example: {},
                planError: null,
                activeStatus: 'Plan is Active (deactivate to edit)',
                sub_one: {
                    active: false,
                    subscriptionStatus: 'Subscription Plan Inactive'
                },
                sub_two: {
                    active: false,
                    subscriptionStatus: 'Subscription Plan Inactive'
                },
                sub_three: {
                    active: false,
                    subscriptionStatus: 'Subscription Plan Inactive'
                },
                sub_four: {
                    active: false,
                    subscriptionStatus: 'Subscription Plan Inactive'
                }
            };

            $scope.checkConnected = function (instance, vm) {
                var Connected = $pageGuard.accountConnected(instance, vm);
                Connected.then(function (result) {
                    $scope.subscriptions.PayPalIsConnected = result.PayPalIsConnected;
                    $scope.subscriptions.PremiumApp = result.PremiumApp;
                    $scope.subscriptions.example.email = result.example;
                    $scope.subscriptions.example.password = result.example.split('@')[0];
                });
            };

            $scope.getSettings = function (instance, vm) {
                var setRet = $pageGuard.retrieveSettings(instance, vm);
                setRet.then(function (result) {
                    $wixForPage.Settings.getSitePages(function (sp) {
                        $scope.viewVars.allPages = sp;
                        $scope.viewVars.mainPage = sp[0];
                        $scope.$apply();
                        var splitResult = $scope.setActivePlans(result, 1);
                        $wixForPage.UI.initialize(splitResult.wixresult);
                        $scope.setActivePlans(splitResult.myresult, 2);
                    });
                    $scope.checkConnected($scope.instance);
                });
            };

            $scope.setActivePlans = function (r, i) {
                var myRes = {};
                angular.forEach(r, function (v, k) {
                    if (i == 2 && k.slice(-6) === 'active' && parseInt(v) === 1) {
                        $scope.setDisplayModelsForActivePlan(k.slice(0, k.length - 7));
                    }
                    if (i == 1 && k.slice(-6) === 'active') {
                        myRes[k] = v;
                        delete r[k];
                    }
                });
                return {
                    wixresult: r,
                    myresult: myRes
                };
            };

            $scope.connectOnClose = function () {
                if ($scope.connect_window.closed) {
                    $scope.checkConnected($scope.instance, $scope.viewModeCheck);
                    $scope.$apply();
                    clearInterval($scope.close_timer);
                }
            };

            $scope.paypalConnect = function () {

                //$wix.Settings.openModal($scope.baseURL + '/paypal/givepermission/'+$wix.Utils.getUid(), 825, 602, "Connect Account", $scope.connectOnClose, true); 
                $scope.connect_window = window.open($scope.baseURL + '/paypal/givepermission/' + $wixForPage.Utils.getUid(),
                        'connect',
                        'toolbars=0,width=825,height=602,left=50,top=50,scrollbars=1,resizable=1');
                $scope.close_timer = setInterval($scope.connectOnClose, 1000);
            };

            $scope.paypalDisconnect = function () {
                $pageGuard.disconnectAccount($scope.instance, $scope.viewModeCheck).then(function () {
                    $scope.checkConnected($scope.instance, $scope.viewModeCheck);
                    $scope.$apply();
                });
            };

            $scope.showUpgrade = function () {
                $wixForPage.openBillingPage();
            };

            $scope.setDisplayModelsForActivePlan = function (strPlan) {
                $scope.subscriptions[strPlan].subscriptionStatus = $scope.subscriptions.activeStatus;
                $scope.subscriptions[strPlan].active = true;
                $wixForPage.UI.set('display_' + strPlan + '_name', $wixForPage.UI.get(strPlan + '_name'));
                $wixForPage.UI.set('display_' + strPlan + '_currency', $wixForPage.UI.get(strPlan + '_currency').value);
                var p = $wixForPage.UI.get(strPlan + '_period').value;
                var period = '';
                switch (p) {
                    case 'D':
                        period = 'Daily';
                        break;
                    case 'W':
                        period = 'Weekly';
                        break;
                    case 'M':
                        period = "Monthly";
                        break;
                    case 'Y':
                        period = "Yearly";
                        break;
                }
                $wixForPage.UI.set('display_' + strPlan + '_period', period);
                $wixForPage.UI.set('display_' + strPlan + '_price', $wixForPage.UI.get(strPlan + '_price'));
            };

            $scope.activatePlan = function (strPlan) {
                var ui_vals = $wixForPage.UI.toJSON();
                var plan_form = pickPlanVals(ui_vals, strPlan);
                var valid = true;
                if (plan_form[strPlan + '_name'] === '') {
                    flashError('#' + strPlan + '_name_div > :input');
                    valid = false;
                }
                var n = parseFloat(plan_form[strPlan + '_price']);
                if (plan_form[strPlan + '_price'] === '' ||
                        isNaN(parseFloat(plan_form[strPlan + '_price']))) {
                    flashError('#' + strPlan + '_price_div > :input');
                    valid = false;
                }
                if (valid) {
                    $pageGuard.activatePlan(strPlan, $scope.instance, $scope.viewModeCheck).success(function (res) {
                        if (res.success === 'true') {
                            $scope.setDisplayModelsForActivePlan(strPlan);
                        } else {
                            $scope.subscriptions.planError = 'There was a plan activation error. You may try again.';
                            setInterval(function () {
                                $scope.subscriptions.planError = null;
                                $scope.$apply();
                            }, 4000);
                        }
                    }).error(function (res) {
                        $scope.subscriptions.planError = 'There was a plan activation error. You may try again.';
                        setInterval(function () {
                            $scope.subscriptions.planError = null;
                            $scope.$apply();
                        }, 4000);
                    });
                }
            };

            $scope.deActivatePlan = function (strPlan) {
                $pageGuard.deActivatePlan(strPlan, $scope.instance, $scope.viewModeCheck).success(function (res) {
                    if (res.success === 'true') {
                        $scope.subscriptions[strPlan].subscriptionStatus = 'Subscription Plan Inactive';
                        $scope.subscriptions[strPlan].active = false;
                    } else {
                        $scope.subscriptions.planError = 'There was a plan activation error. You may try again.';
                        setInterval(function () {
                            $scope.subscriptions.planError = null;
                            $scope.$apply();
                        }, 4000);
                    }
                }).error(function (res) {
                    $scope.subscriptions.planError = 'There was a plan activation error. You may try again.';
                    setInterval(function () {
                        $scope.subscriptions.planError = null;
                        $scope.$apply();
                    }, 4000);
                });
            };

            function flashError(sel) {
                var mm = $(sel).css('border-color');
                $(sel).css('border-color', 'red');
                setInterval(function () {
                    $(sel).css('border-color', mm);
                }, 1500);
            }

            function pickPlanVals(ui_vals, strPlan) {
                var planVals = {};
                angular.forEach(ui_vals, function (v, k) {
                    if (k.substr(0, strPlan.length) === strPlan) {
                        planVals[k] = v;
                    }
                });
                return planVals;
            }

            $scope.viewModeCheck = $wixForPage.Utils.getViewMode();

            $scope.runOnce = false;
            if ($scope.viewModeCheck !== 'standalone' && $scope.runOnce === false) {
                $scope.runOnce = true;
                $scope.instanceId = $wixForPage.Utils.getInstanceId();
                $scope.instance = $wixForPage.Utils.getInstance();
                $scope.getSettings($scope.instance, $scope.viewModeCheck);
            }

            $wixForPage.UI.onChange('*', function () {
                $pageGuard.persistSettings($wixForPage.UI.toJSON(), $scope.instance, $scope.viewModeCheck);
                $wixForPage.Settings.triggerSettingsUpdatedEvent('updated', $wixForPage.Utils.getOrigCompId());
            });
        });