'use strict';
angular.module('pageGuard', []);
angular.module('pageGuard').factory('$pageGuard', function ($http) {
    var sitePages = {tag: 'value'};
    var authUser = {uid: 0};
    var service = {
        hello: function () {
            console.log('hello from the pageGuard service');
        },
        accountConnected: function (scopeInstance, scopevm) {
            var r = $http.get('/accountConnected', {headers:
                        {'X-WIX-INSTANCE': scopeInstance,
                            'X-VIEW-MODE': scopevm
                        }}).then(function (res) {
                return res.data.connected;
            }, function (res) {
                return false;
            });
            return r;
        },
        disconnectAccount: function (scopeInstance, scopevm) {
            return $http.get('/disconnectAccount', {headers:
                        {'X-WIX-INSTANCE': scopeInstance,
                            'X-VIEW-MODE': scopevm
                        }});
        },
        persistSettings: function (json, scopeInstance, scopevm) {
            $http.post('/saveSettings', json, {headers:
                        {'X-WIX-INSTANCE': scopeInstance,
                            'X-VIEW-MODE': scopevm
                        }});
        },
        retrieveSettings: function (scopeInstance, scopevm) {
            return $http.get('/getSettings', {headers:
                        {'X-WIX-INSTANCE': scopeInstance,
                            'X-VIEW-MODE': scopevm
                        }}).then(function (res) {
                angular.forEach(res.data, function (rv, rk) {
                    if (rk.substr(0, 5) === 'page_') {
                        sitePages[rk.substr(5)] = rv === true ? 'guarded' : 'public';
                        return;
                    }
                });
                return res.data;
            }, function (res) {
                return false;
            });
        },
        retrievePlans: function (scopeInstance) {
            return $http.get('/getActivePlans', {headers:
                        {'X-WIX-INSTANCE': scopeInstance
                        }});
        },
        retrieveSubscriptions: function (scopeInstance, scopevm) {
            return $http.get('/getActiveSubscriptions', {headers:
                        {'X-WIX-INSTANCE': scopeInstance,
                            'X-VIEW-MODE': scopevm
                        }});
        },
        checkRestricted: function (id) {
            if (sitePages[id]) {
                return sitePages[id] === 'guarded';
            }
        },
        checkCredentials: function (u, p, scopeInstance) {
            var inp = {'email': u, 'password': p};
            return $http.post('/getUser', inp, {headers:
                        {'X-WIX-INSTANCE': scopeInstance
                        }});
        },
        checkRemUser: function (scopeInstance, u) {
            var inp = {'wix_uid': u};
            return $http.post('/getRemUser', inp, {headers:
                        {'X-WIX-INSTANCE': scopeInstance
                        }});
        },
        checkEmailExists: function (e, scopeInstance) {
            var inp = {'email': e};
            return $http.post('/getEmail', inp, {headers:
                        {'X-WIX-INSTANCE': scopeInstance
                        }});
        },
        passwordreset: function (e, scopeInstance) {
            var inp = {'email': e};
            return $http.post('/resetPassword', inp, {headers:
                        {'X-WIX-INSTANCE': scopeInstance
                        }});
        },
        setNewUser: function (u, p, scopeInstance) {
            var inp = {'email': u, 'password': p};
            return $http.post('/setNewUser', inp, {headers:
                        {'X-WIX-INSTANCE': scopeInstance
                        }});
        },
        setAuthUser: function (uid) {
            authUser.uid = uid;
        },
        getAuthUser: function () {
            return authUser;
        },
        activatePlan: function (strPlan, scopeInstance, scopevm) {
            return $http.post('/settings/activatePlan', {opt_num: strPlan}, {headers:
                        {'X-WIX-INSTANCE': scopeInstance,
                            'X-VIEW-MODE': scopevm
                        }});
        },
        deActivatePlan: function (strPlan, scopeInstance, scopevm) {
            return $http.post('/settings/activatePlan', {opt_num: strPlan, active: 'false'}, {headers:
                        {'X-WIX-INSTANCE': scopeInstance,
                            'X-VIEW-MODE': scopevm
                        }});
        }
    };
    return service;
});


