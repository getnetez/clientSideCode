'use strict';

angular.module('wixForWorker', []);

angular.module('wixForWorker').factory('$wixForWorker', function ($location) {
  var url = $location.absUrl();

  Wix.Worker.Utils.getInstance = function() {
    var instanceRegexp = /.*instance=([\[\]a-zA-Z0-9\.\-_]*?)(&|$|#).*/g;
    var instance = instanceRegexp.exec(url);
    if (instance && instance[1]) {
      return instance[1];
    } else {
      return undefined;
    }
  };
  return Wix;
});