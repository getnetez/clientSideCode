'use strict';

angular.module('wixSubscription', [
    'ngRoute',
    'ngSanitize',
    'wixForPage',
    'pageGuard',
    'LocalStorageModule'
])
        .config(function ($routeProvider) {
            $routeProvider
                    .when('/', {
                        templateUrl: '/views/subscription.html',
                        controller: 'MainCtrl'
                    })
                    .when('/login', {
                        templateUrl: '/views/login.html',
                        controller: 'MainCtrl'
                    })
                    .when('/signup', {
                        templateUrl: '/views/signup.html',
                        controller: 'MainCtrl'
                    })
                    .when('/forgot', {
                        templateUrl: '/views/forgot.html',
                        controller: 'MainCtrl'
                    })
                    .otherwise({
                        redirectTo: '/'
                    });
        }).config(['localStorageServiceProvider', function (localStorageServiceProvider) {
        localStorageServiceProvider.setPrefix('pgls');
    }]);

