'use strict';

angular.module('wixApp', [
    'ngRoute',
    'wixForWorker',
    'pageGuard'
])
        .config(function ($routeProvider) {
            $routeProvider
                    .when('/', {
                        templateUrl: 'views/app.html',
                        controller: 'MainCtrl'
                    })
                    .otherwise({
                        redirectTo: '/'
                    });
        });
        